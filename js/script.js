// 1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// ОТВЕТ:
// var обьявляет переменную глобально во всей функции/коде тогда как let обьявляет переменную только в блоке локально,
// другими словами разница в области видимости переменной
//2. Почему объявлять переменную через var считается плохим тоном?
//ОТВЕТ:
//Из-за глобальной видимости var-переменной и наличия большого их количества в скрипте
//могут возникнуть трудности с пересечением вышеперечисленных.
//Удобно пользоваться переменными, обьявляя их локально в блоках.

//ЗАДАНИЕ
// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//    - Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//    - Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//    - Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
//    - Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

let userName = prompt("Enter your NAME");
while (Boolean(userName) === false || !isNaN(userName)) {
    userName = prompt("Wrong data. Enter your NAME again!", "")

}
let userAge = +prompt("Enter your AGE");
while (Boolean(userAge) === false || isNaN(userAge)) {
    userAge = +prompt("Wrong data. Enter your AGE again!")
}

if (userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    let userAnswer = confirm("Are you sure you want to continue?")
    if (userAnswer === true) {
        alert("Welcome" + " " + userName + "!");
    } else {
        alert("You are not allowed to visit this website")
    }
} else if (userAge > 22) {
    alert("Welcome" + " " + userName + "!")
}